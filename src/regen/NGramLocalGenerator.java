/*
 * Copyright 2016 by the House Stark contributors
 * SPDX-License-Identifier: CC0-1.0
 */
package regen;

import java.util.Arrays;

/**
 *
 * @author msto
 */
public class NGramLocalGenerator extends NGramGenerator {

    private int[] trigram_table;
    private int[] background_table;

    @Override
    public void runSetup() {
        NGramSource.trigrams.setEnabled(true);
    }

    private double conditionalTrigramProbability(int w1, int center, int w3, int[] fullset) {

        // Try to read from cache.
        int tgpos = w1 * ntrans * ntrans + w3 * ntrans + center;
        if (trigram_table[tgpos] == 0) {
            // Fill cache
            int base = 0;
            for (int j = 0; j < fullset.length; j++) {
                int cwd = fullset[j];
                int adjcount = NGramSource.trigrams.getOrDefault(transTable[w1], transTable[cwd], transTable[w3], 0) + 1;
                int sbpos = w1 * ntrans * ntrans + w3 * ntrans + cwd;
                trigram_table[sbpos] = adjcount;
                base += adjcount;
            }
            background_table[w1 * ntrans + w3] = base;
        }
        int top = trigram_table[tgpos];
        int bot = background_table[w1 * ntrans + w3];
        double condprob = (double) top / (double) bot;
        return condprob;
    }

    @Override
    public void setSource(String original) {
        super.setSource(original);
        // TODO: Share caches, and create a request system so we don't
        // unnecessarily load quatrigrams

        // Maximum memory use is (len)^3 which, given that sentences of
        // 200-unique words are really rare, isn't bad at all.
        trigram_table = new int[ntrans * ntrans * ntrans];
        background_table = new int[ntrans * ntrans];
    }

    @Override
    public String next() {
        if (N == 1) {
            return given[0];
        }

        // Set start/end points
        WordSet words = new WordSet(wordlist);
        /* Results are generated anew _every time_ */
        int[] result = new int[N];
        Arrays.fill(result, -11);// < killme
        int vlead, vtrail;
        if (nleaders == 0 || ntrailers == 0) {
            throw new IllegalStateException("All word sets must have either leading/trailing words.");
        }
        do {
            vlead = leaders[Util.RAND.nextInt(nleaders)];
            vtrail = trailers[Util.RAND.nextInt(ntrailers)];
        } while (vlead == vtrail && words.countWord(vlead) < 2);

        /* Select a leading word (have to do it first...) */
        result[0] = words.takeWord(vlead);
        result[N - 1] = words.takeWord(vtrail);
        for (int i = 1; i < N - 1; i++) {
            result[i] = words.takeIndex(Util.RAND.nextInt(words.left));
        }
        // We have a candidate!
        // Apply random pairwise swaps proportionally to length,
        // only if they improve scores.
        // Note the (low) odds of unchanged results.
        int limit = Util.RAND.nextInt(20) * (N - 2);
        for (int k = 0; k < limit; k++) {
            int from = Util.RAND.nextInt(N - 2) + 1;
            int to = Util.RAND.nextInt(N - 2) + 1;
            if (from == to) {
                continue;
            }
            // Yes, double calculating...
            double orig1 = conditionalTrigramProbability(result[from - 1], result[from], result[from + 1], result);
            double orig2 = conditionalTrigramProbability(result[to - 1], result[to], result[to + 1], result);
            double swap1 = conditionalTrigramProbability(result[from - 1], result[to], result[from + 1], result);
            double swap2 = conditionalTrigramProbability(result[to - 1], result[from], result[to + 1], result);
            if (swap2 * swap1 > orig1 * orig2) {
                int tmp = result[from];
                result[from] = result[to];
                result[to] = tmp;
            }
        }

        return translateOut(result);
    }
}
