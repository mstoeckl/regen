/*
 * Copyright 2016 by the House Stark contributors
 * SPDX-License-Identifier: CC0-1.0
 */
package regen;

import java.io.IOException;
import java.lang.reflect.Field;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import sun.misc.Unsafe;

/**
 *
 * @author msto
 */
public class Util {

    public static final String DATA_PATH = System.getenv("GRAVEYARD") + "/";
    public static final Random RAND = new Random();

    public static final int N_RESERVED_IDS = 256;

    public static final Unsafe UNSAFE = getUnsafe();

    private static Unsafe getUnsafe() {
        try {
            Field singleoneInstanceField = Unsafe.class
                    .getDeclaredField("theUnsafe");
            singleoneInstanceField.setAccessible(true);
            return (Unsafe) singleoneInstanceField.get(null);
        } catch (NoSuchFieldException | SecurityException |
                IllegalArgumentException | IllegalAccessException ex) {
            System.err.println("Couldn't load the unsafe!");
            return null;
        }
    }

    public static final int PUNCT_ID = 10;
    public static final int ADJ_ID = 11;
    public static final int ADP_ID = 12;
    public static final int ADV_ID = 13;
    public static final int CONJ_ID = 14;
    public static final int DET_ID = 15;
    public static final int NOUN_ID = 16;
    public static final int NUM_ID = 17;
    public static final int PRON_ID = 18;
    public static final int PRT_ID = 19;
    public static final int VERB_ID = 20;
    public static final int X_ID = 21;

    public static final int HEAD_ID = 99;
    public static final int TAIL_ID = 100;

    public static boolean isTerminal(String s) {
        switch (s.charAt(s.length() - 1)) {
            case '.':
            case '!':
            case '?':
                return true;
            case '"':
            case '\'':
            case ')':
                /* dealing with closer elements */
                return s.length() <= 1 ? false : isTerminal(s.substring(0, s.length() - 1));
            default:
                return false;
        }
    }

    public static boolean isInitial(String s) {
        // warning: need to string quotes/parens.
        boolean leadCap = ('A' <= s.charAt(0) && s.charAt(0) <= 'Z');
        if (leadCap) {
            return true;
        }
        if (s.length() > 1 && ('(' == s.charAt(0) || '"' == s.charAt(0))) {
            return isInitial(s.substring(1));
        }
        // accept digits as leads.
        return '0' <= s.charAt(0) && s.charAt(0) <= '9';
    }

    public static String[] shuffledOf(String[] given) {
        int[] idx = new int[given.length];
        for (int j = 0; j < idx.length; j++) {
            idx[j] = j;
        }
        shuffle(idx);
        String[] rando = new String[given.length];
        for (int j = 0; j < idx.length; j++) {
            rando[j] = given[idx[j]];
        }
        return rando;
    }

    public static void shuffle(int[] a) {
        /* Ye olde shuffle */
        for (int i = a.length - 1; i >= 0; i--) {
            int j = RAND.nextInt(i + 1);
            int t = a[j];
            a[j] = a[i];
            a[i] = t;
        }
    }

    /**
     * Pick an item from a CDF. CDF Format is:
     *
     * [0, A, B, C, D] (items = 4)
     *
     * where A=0+P(0), B=A+P(1), ...
     *
     * @param cdf
     * @param items
     * @return
     */
    public static int selectFromCDF(int[] cdf, int items) {
        int idx = RAND.nextInt(cdf[items]);
        for (int j = 0; j < items; j++) {
            if (idx < cdf[j + 1]) {
                return j;
            }
        }
        throw new IllegalStateException("CDF badly ordered");
    }

    public static <T> T croak(String s, Object... x) {
        System.err.printf(s + "\n", x);
        System.exit(1);
        return null;
    }

    public static String[][] loadTestInput(String fname) {
        ArrayList<String[]> sents = new ArrayList<>();
        try {
            List<String> l = Files.readAllLines(Paths.get(fname));
            ArrayList<String> substr = new ArrayList<>();
            for (String q : l) {
                if (q.contains("=====")) {
                    if (substr.size() > 0) {
                        String[] s = new String[substr.size()];
                        substr.toArray(s);
                        substr.clear();
                        sents.add(s);
                    }
                } else {
                    for (String word : q.split("\\s+")) {
                        if (!word.isEmpty()) {
                            substr.add(word);
                        }
                    }
                }
            }
        } catch (IOException ex) {
            return Util.croak("Err: reading shuffled input (%s) failed.", fname);
        }

        return sents.toArray(new String[0][]);
    }

    public static boolean isTerminal(byte[] s) {
        switch (s[s.length - 1]) {
            case '.':
            case '!':
            case '?':
                return true;
            case '"':
            case '\'':
            case ')':
                /* dealing with closer elements */
                return s.length <= 1 ? false : isTerminal(Arrays.copyOf(s, s.length - 1));
            default:
                return false;
        }
    }
}
