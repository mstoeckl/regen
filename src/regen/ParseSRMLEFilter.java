/*
 * Copyright 2016 by the House Stark contributors
 * SPDX-License-Identifier: CC0-1.0
 */
package regen;

import edu.stanford.nlp.ling.HasWord;
import edu.stanford.nlp.ling.TaggedWord;
import edu.stanford.nlp.parser.lexparser.IntDependency;
import edu.stanford.nlp.parser.lexparser.LexicalizedParser;
import edu.stanford.nlp.parser.lexparser.MLEDependencyGrammar;
import edu.stanford.nlp.parser.lexparser.TreeAnnotator;
import edu.stanford.nlp.parser.shiftreduce.ShiftReduceParser;
import edu.stanford.nlp.parser.shiftreduce.ShiftReduceParserQuery;
import edu.stanford.nlp.process.Tokenizer;
import edu.stanford.nlp.process.TokenizerFactory;
import edu.stanford.nlp.tagger.common.Tagger;
import edu.stanford.nlp.trees.CollinsHeadFinder;
import edu.stanford.nlp.trees.Tree;
import java.io.StringReader;
import java.util.List;

/**
 *
 * @author jake
 */
public class ParseSRMLEFilter implements Regen.Filter {

    private static ShiftReduceParser srpBeam = null;
    private static LexicalizedParser lp = null;
    private static Tagger tagger = null;
    private static TokenizerFactory<? extends HasWord> tf = null;
    private static CollinsHeadFinder finder = null;
    private static TreeAnnotator anno = null;

    public void runSetup() {
        if (srpBeam == null) {
            srpBeam = ShiftReduceParser.loadModel("edu/stanford/nlp/models/srparser/englishSR.beam.ser.gz");
            lp = LexicalizedParser.loadModel("edu/stanford/nlp/models/lexparser/englishFactored.ser.gz");
            tagger = Tagger.loadModel("edu/stanford/nlp/models/pos-tagger/english-left3words/english-left3words-distsim.tagger");
            tf = srpBeam.treebankLanguagePack().getTokenizerFactory();
            finder = new CollinsHeadFinder(lp.treebankLanguagePack());
            anno = new TreeAnnotator(finder, lp.getTLPParams(), lp.getOp());
        }
    }

    @Override
    public double score(String sentence, boolean dummy) {
        Tokenizer<? extends HasWord> f = tf.getTokenizer(new StringReader(sentence));
        List<? extends HasWord> s = f.tokenize();
        List<TaggedWord> tagged = tagger.apply(s);
        // Parse pretagged sentence
        ShiftReduceParserQuery srpq = (ShiftReduceParserQuery) srpBeam.parserQuery();
        Tree tree = null;
        if (srpq.parse(tagged)) {
            tree = srpq.getBestBinarizedParse();
        } else {
            return Double.NEGATIVE_INFINITY;
        }
        // Analyze best result
        anno.transformTree(tree);

        List<IntDependency> deps = MLEDependencyGrammar.treeToDependencyList(tree, lp.wordIndex, lp.tagIndex);
        MLEDependencyGrammar mle = (MLEDependencyGrammar) lp.dg;
        double score = mle.scoreAll(deps);
        System.err.printf("%f %s\n", score, sentence);
        return score;
    }

    @Override
    public String shortName() {
        return "Shift Reduce Parse";
    }
}
