/*
 * Copyright 2016 by the House Stark contributors
 * SPDX-License-Identifier: CC0-1.0
 */
package regen;

import java.util.Arrays;

/**
 * Generate n-gram-search derived word sequences.
 *
 * @author msto
 */
public class NGramGenerator implements Regen.Generator {

    protected int N;
    protected int[] wordlist;
    protected int[] paralist;
    protected int ntrans;
    protected int nleaders;
    protected int[] leaders;
    protected int ntrailers;
    protected int[] trailers;
    protected int[] transTable;
    protected String[] given;

    @Override
    public void runSetup() {
        NGramSource.bigrams.setEnabled(true);
    }

    @Override
    public void setSource(String original) {
        given = original.split(" ");
        N = given.length;
        /* List of integers indicating transTable index */
        wordlist = new int[N];
        /* List of integers indexing back on the original string */
        paralist = new int[N];
        /* List of integers indicating a unique word's translation */
        ntrans = 0;
        transTable = new int[N];
        Arrays.fill(transTable, -16777216);
        /* List of transTable indices which could lead a sentence */
        nleaders = 0;
        leaders = new int[N];
        Arrays.fill(leaders, -13);// < killme
        /* List of transTable indices which could END a sentence */
        ntrailers = 0;
        trailers = new int[N];
        Arrays.fill(trailers, -12);// < killme
        /* Fill! */
        for (int i = 0; i < N; i++) {
            String s = given[i];
            int v = -1;
            int w = -1;
            // Identity check uses original strings (good!)
            for (int j = 0; j < i; j++) {
                if (given[j].equals(s)) {
                    v = wordlist[j];
                    break;
                }
            }
            if (v >= 0) {
                wordlist[i] = v;
                continue;
            }
            wordlist[i] = ntrans;
            paralist[ntrans] = i;
            // Translation list may reduce strings down (still good!)
            byte[] sb = NGramSource.downCastWord(s, true);
            transTable[ntrans] = NGramSource.intLookup.getOrDefault(sb, -i - 1);
            if (transTable[ntrans] < 0) {
                // TODO: Log in the IntLookup itself,
//                idkSet.add(s);
            }
            if (Util.isInitial(s)) {
                leaders[nleaders] = ntrans;
                nleaders++;
            }
            if (Util.isTerminal(sb)) {
                trailers[ntrailers] = ntrans;
                ntrailers++;
            }
            ntrans++;
        }

        // We have none of either, so let all words be!
        if (nleaders == 0) {
            System.arraycopy(wordlist, 0, leaders, 0, N);
            nleaders = N;
        }
        if (ntrailers == 0) {
            System.arraycopy(wordlist, 0, trailers, 0, N);
            ntrailers = N;
        }

        // Just in case
        Util.shuffle(wordlist);
    }

    String translateOut(int[] result
    ) {
        String[] real = new String[N];
        for (int j = 0; j < N; j++) {
//            int wordval =
            // paralist maps the (deduplicated) codes of result onto
            // locations in th original string.
            real[j] = given[paralist[result[j]]];
//
//            int wordval = transTable[result[j]];
//            if (wordval < 0) {
//                real[j] = given[-wordval - 1];
//            } else {
//                real[j] = new String(NGramSource.wordLookup.get(wordval - Util.N_RESERVED_IDS), StandardCharsets.US_ASCII);
//            }
        }
        return String.join(" ", real);
    }

    @Override
    public String next() {
        if (N == 1) {
            return given[0];
        }

        // Currently using bigram bidirectional tree search
        int[] cm_weights = new int[N + 1];
        WordSet words = new WordSet(wordlist);
        /* Results are generated anew _every time_ */
        int[] result = new int[N];
        Arrays.fill(result, -11);// < killme

        /* Select leading/trailing words which are _not_ the same. Assumes
               there are any and enough.*/
        int vlead, vtrail;
        do {
            vlead = leaders[Util.RAND.nextInt(nleaders)];
            vtrail = trailers[Util.RAND.nextInt(ntrailers)];
        } while (vlead == vtrail && words.countWord(vlead) < 2);

        /* Select a leading word (have to do it first...) */
        result[0] = words.takeWord(vlead);
        result[N - 1] = words.takeWord(vtrail);

        int mid = N / 2;

        /* Select new words based on the previous word */
        for (int i = 1; i < mid; i++) {
            int prev = result[i - 1];
            int pwd = transTable[prev];
            if (pwd < 0) {
                /* Pick next word at random */
                result[i] = words.takeIndex(Util.RAND.nextInt(words.left));
            } else {
                // Pick weighted by condition frequencies of each, times
                // the frequency with which it is left
                cm_weights[0] = 0;
                for (int j = 0; j < words.left; j++) {
                    int nwd = transTable[words.data[j]];
                    /* ensure some chance of picking any value */
                    int s = NGramSource.bigrams.getOrDefault(pwd, nwd, 1);;
                    cm_weights[j + 1] = cm_weights[j] + s;
                }
                int idx = Util.selectFromCDF(cm_weights, words.left);
                result[i] = words.takeIndex(idx);
            }
        }

        /* Select new words based on the following word
             * Yes, this is basically a carbon copy. */
        for (int i = N - 2; i >= mid; i--) {
            int nwd = transTable[result[i + 1]];
            if (nwd < 0) {
                /* Pick next word at random */
                result[i] = words.takeIndex(Util.RAND.nextInt(words.left));
            } else {
                cm_weights[0] = 0;
                for (int j = 0; j < words.left; j++) {
                    int pwd = transTable[words.data[j]];
                    /* ensure some chance of picking any value */
                    int s = NGramSource.bigrams.getOrDefault(pwd, nwd, 1);;
                    cm_weights[j + 1] = cm_weights[j] + s;
                }
                int idx = Util.selectFromCDF(cm_weights, words.left);
                result[i] = words.takeIndex(idx);
            }
        }

        /* Translate into string-space */
        return translateOut(result);
    }

}
