/*
 * Copyright 2016 by the House Stark contributors
 * SPDX-License-Identifier: CC0-1.0
 */
package regen;

/**
 *
 * @author msto
 */
public class QuadgramFilter implements Regen.Filter {

    private int[] basiscache;
    private int N;
    private int hits = 0;
    private int misses = 0;
    private int doublemisses = 0;

    @Override
    public void runSetup() {
        NGramSource.trigrams.setEnabled(true);
        NGramSource.quatrigrams.setEnabled(true);
    }

    @Override
    public void setSource(String orig) {
        N = orig.split(" ").length;
        basiscache = new int[N * N];
//        System.out.printf("Last: %d %d %d\n", hits, misses, doublemisses);
        hits = 0;
        misses = 0;
        doublemisses = 0;
    }

    private int modifiedQuatrigramCount(int w1, int w2, int w3, int w4) {
        int p1 = NGramSource.trigrams.getOrDefault(w1, w2, w3, 0);
        int p2 = NGramSource.trigrams.getOrDefault(w2, w3, w4, 0);
        int q = NGramSource.quatrigrams.getOrDefault(w1, w2, w3, w4, 0);

        if (q == 0) {
            misses++;
        } else if (p1 == 0 && p2 == 0) {
            doublemisses++;
        } else {
            hits++;
        }

        // I.e, rate the quatrigram itself highly, else default to trigram
        return p1 + p2 + q * 5 + 1;

//        int c = NGramSource.quatrigrams.getOrDefault(w1, w2, w3, w4, 0);
//        if (c > 0) {
//            hits++;
//            // Finder's skew
//            return c + 1;
//        }
//        misses++;
//
//        int p1 = NGramSource.trigrams.getOrDefault(w1, w2, w3, 0);
//        int p2 = NGramSource.trigrams.getOrDefault(w2, w3, w4, 0);
//        int sc = Math.min(p1, p2);
//        if (sc == 0) {
//            doublemisses++;
//        }
//        // 10 being our magic number here; after all, two good trigrams aren't bad
//        return (sc / 10) + 1;
    }

    /**
     * Note that indices into set[] are passed; not word-values themselves.
     *
     * @param z1
     * @param m2
     * @param m3
     * @param z4
     * @param set
     * @return
     */
    private double logConditionalQuatrigramProbability(int z1, int m2, int m3, int z4, int[] set) {
        int loc = z1 * N + z4;
        int basis = basiscache[loc];
        if (basis == 0) {
            for (int i = 0; i < set.length; i++) {
                for (int j = 0; j < set.length; j++) {
                    // We permit duplicate counting for sake of simplicity.
                    // at least it maintains the rank ordering & relativity
                    int c = modifiedQuatrigramCount(set[z1], set[i], set[j], set[z4]);
                    basis += c;
                }
            }
            basiscache[loc] = basis;
        }

        int num = modifiedQuatrigramCount(set[z1], set[m2], set[m3], set[z4]);
        double prob = (double) num / (double) basis;
        return Math.log(prob);
    }

    @Override
    public double score(String sentence, boolean dummy) {
        String[] parts = sentence.split(" ");
        int[] words = new int[parts.length + 2];
        words[0] = Util.HEAD_ID;
        words[words.length - 1] = Util.TAIL_ID;
        for (int i = 0; i < parts.length; i++) {
            byte[] rootwrd = NGramSource.downCastWord(parts[i], true);
            words[i + 1] = NGramSource.intLookup.getOrDefault(rootwrd, -1);
        }
        // Translate into words
        double score = 0.0;
        for (int i = 0; i < parts.length - 3; i++) {
            score += logConditionalQuatrigramProbability(i, i + 1, i + 2, i + 3, words);
        }
        // length-normalize score to allow for reasonable postprocessing
        return score / (double) N;
    }

    @Override
    public String shortName() {
        return "4-gram filter";
    }

}
