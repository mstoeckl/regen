/*
 * Copyright 2016 by the House Stark contributors
 * SPDX-License-Identifier: CC0-1.0
 */
package regen;

import edu.stanford.nlp.ling.HasWord;
import edu.stanford.nlp.ling.TaggedWord;
import edu.stanford.nlp.parser.common.ParserQuery;
import edu.stanford.nlp.parser.lexparser.LexicalizedParser;
import edu.stanford.nlp.trees.Tree;
import edu.stanford.nlp.util.ScoredObject;
import java.util.List;
import java.util.function.Function;

/**
 *
 * @author jake
 */
public class ParseFilter implements Regen.Filter {

    private static LexicalizedParser parser = null;
    private static Function<List<? extends HasWord>, List<TaggedWord>> tagger = null;

    public void runSetup() {
        if (parser == null) {
            // Do work, force class loading.
            parser = LexicalizedParser.loadModel("edu/stanford/nlp/models/lexparser/englishPCFG.ser.gz");
            tagger = parser.loadTagger();
        }
    }

    @Override
    public String shortName() {
        return "Parsing Score";
    }

    public double score(String sentence, boolean dummy) {
        List<? extends HasWord> tokens = parser.tokenize(sentence);
        if (parser.getOp().testOptions.preTag) {
            tokens = tagger.apply(tokens);
        }
        ScoredObject<Tree> tree = null;
        ParserQuery pq = parser.parserQuery();
        if (pq.parse(tokens)) {
            // Parse failures all horrible
            return Double.NEGATIVE_INFINITY;
        }
        tree = pq.getBestPCFGParses().get(0);
        return tree.score();
    }
}
