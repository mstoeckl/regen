/*
 * Copyright 2016 by the House Stark contributors
 * SPDX-License-Identifier: CC0-1.0
 */
package regen;

import java.util.Arrays;

/**
 *
 * @author msto
 */
public class WordSet {

    public final int[] data;
    public int left;

    WordSet(int[] source_to_copy) {
        data = Arrays.copyOf(source_to_copy, source_to_copy.length);
        left = source_to_copy.length;
    }

    /**
     * Take a word from the WordSet, reducing the remaining set of words by one.
     *
     * @param value
     * @return value
     */
    int takeWord(int value) {
        /* Q: how to avoid this scan? */
        for (int i = 0; i < left; i++) {
            if (data[i] == value) {
                return takeIndex(i);
            }
        }
        throw new IllegalStateException("Could not find word");
    }

    /**
     * Take a word at a given index and return its value.
     *
     * @param idx
     * @return value
     */
    int takeIndex(int idx) {
        int value = data[idx];
        left--;
        data[idx] = data[left];
        data[left] = -1;// < killme
        return value;
    }

    int countWord(int value) {
        int count = 0;
        for (int i = 0; i < left; i++) {
            if (data[i] == value) {
                count++;
            }
        }
        return count;
    }
}
