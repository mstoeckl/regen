/*
 * Copyright 2016 by the House Stark contributors
 * SPDX-License-Identifier: CC0-1.0
 */
package regen;

/**
 *
 * @author msto
 */
public class TrigramFilter implements Regen.Filter {

    public TrigramFilter() {
    }

    @Override
    public void runSetup() {
        NGramSource.trigrams.setEnabled(true);
    }

    public double score(String sentence, boolean dummy) {
        String[] parts = sentence.split(" ");
        int[] words = new int[parts.length + 2];
        words[0] = Util.HEAD_ID;
        words[words.length - 1] = Util.TAIL_ID;
        for (int i = 0; i < parts.length; i++) {
            byte[] rootwrd = NGramSource.downCastWord(parts[i], true);
            words[i + 1] = NGramSource.intLookup.getOrDefault(rootwrd, -1);
        }

        double score = 0.0;
        /* Note: May want to direct-cache the conditional probabilities. Only O(N^3) memory! */
        for (int i = 1; i < parts.length + 1; i++) {
            /* Calculate conditional probability of a word given other set.
             * We add one to all scores, skewing slightly but maintaining rank */
            int pin = NGramSource.trigrams.getOrDefault(words[i - 1], words[i], words[i + 1], 0) + 1;
            int base = pin;
            /* We skip the two words flanking position i */
            for (int j = 1; j < i - 1; j++) {
                base += NGramSource.trigrams.getOrDefault(words[i - 1], words[j], words[i + 1], 0) + 1;
            }
            for (int j = i + 2; j < parts.length + 1; j++) {
                base += NGramSource.trigrams.getOrDefault(words[i - 1], words[j], words[i + 1], 0) + 1;
            }

            double condprob = (double) pin / (double) base;
            score += Math.log(condprob);
        }
        return score;
    }

    @Override
    public String shortName() {
        return "Trigram Probability";
    }
}
