/*
 * Copyright 2016 by the House Stark contributors
 * SPDX-License-Identifier: CC0-1.0
 */
package regen;

import edu.stanford.nlp.ling.HasWord;
import edu.stanford.nlp.ling.TaggedWord;
import edu.stanford.nlp.parser.shiftreduce.ShiftReduceParser;
import edu.stanford.nlp.parser.shiftreduce.ShiftReduceParserQuery;
import edu.stanford.nlp.process.Tokenizer;
import edu.stanford.nlp.process.TokenizerFactory;
import edu.stanford.nlp.tagger.common.Tagger;
import java.io.Reader;
import java.io.StringReader;
import java.util.List;

/**
 *
 * @author jake
 */
public class ParseSRFilter implements Regen.Filter {

    private static ShiftReduceParser srpBeam = null;
    private static Tagger tagger = null;
    private static TokenizerFactory<? extends HasWord> tf;

    public void runSetup() {
        if (srpBeam == null) {
            srpBeam = ShiftReduceParser.loadModel("edu/stanford/nlp/models/srparser/englishSR.beam.ser.gz");
            tagger = Tagger.loadModel("edu/stanford/nlp/models/pos-tagger/english-left3words/english-left3words-distsim.tagger");
            tf = srpBeam.treebankLanguagePack().getTokenizerFactory();
        }
    }

    @Override
    public double score(String sentence, boolean dummy) {
        Reader r = new StringReader(sentence);
        Tokenizer<? extends HasWord> f = tf.getTokenizer(r);
        List<? extends HasWord> s = f.tokenize();
        List<TaggedWord> t = tagger.apply(s);
        ShiftReduceParserQuery srpq = (ShiftReduceParserQuery) srpBeam.parserQuery();
        if (srpq.parse(t)) {
            return srpq.getPCFGScore();
        } else {
            return Double.NEGATIVE_INFINITY;
        }
    }

    @Override
    public String shortName() {
        return "Shift Reduce Parse";
    }

}
