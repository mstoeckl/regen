/*
 * Copyright 2016 by the House Stark contributors
 * SPDX-License-Identifier: CC0-1.0
 */
package regen;

import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;
import java.util.zip.GZIPInputStream;

/**
 * NGram sources/corpora/intermediates. Class exists to centralize NGram
 * loading, and make it easier to handle the ten+ possible corpora.
 *
 * @author msto
 */
public class NGramSource {

    public static final MapBytesToInt intLookup = new MapBytesToInt();
    public static final List<byte[]> wordLookup = new ArrayList<>();
    public static final NGramTable unigrams = new NGramTable(1);
    public static final NGramTable bigrams = new NGramTable(2);
    public static final NGramTable trigrams = new NGramTable(3);
    public static final NGramTable quatrigrams = new NGramTable(4);

    private static final NGramLoader[] opts = {
        new CachedNGramSource(new TableNGramSource("coha", "Corpus of Historical American English (440M words)")),
        new TableNGramSource("coca", "Corpus of Contemporary American English, Top 1M (520M words)"),
        new CorpusNGramSource("oanc", "Open American National Corpus (15M words)"),
        new CorpusNGramSource("guten", "Cleaned Subset of Project Gutenberg (200M words)"),
        new CachedNGramSource(new CorpusNGramSource("newscom", "News Commentary corpus (3M words)")),
        new CorpusNGramSource("aclarc", "ACL Anthology Reference Corpus (40M words?)"),
        new CorpusNGramSource("bnc", "British National Corpus (100M words)"),
        new CachedNGramSource(new CorpusNGramSource("craft", "CRAFT Corpus (400K words)")),
        new CachedNGramSource(new TaggedCorpusSource("tagoanc", "Open American National Corpus (15M words)")),
        new TaggedCorpusSource("tagbnc", "British National Corpus (100M words)"),
        new TaggedCorpusSource("wikipedia", "2006 Wikipedia (500M words)"),
        new TaggedCorpusSource("wikipedia", "wiki10M", "2006 Wikipedia (first 10M words)", 10000000),
        new CachedNGramSource(new SemiFinalNGramSource("tagsemifinal", "American & British National Corpus (115M words)")),
        new FinalNGramSource("tagfinal", "ACLARC & Wikipedia & American & British National Corpus (655M words)")};
    private static final boolean[] legal = new boolean[256];

    static {
        Pattern good = Pattern.compile("[a-zA-Z0-9,;:/$%\\-\\.\\(\\)\\?\\!'\"]");
        for (int i = 0; i < 256; i++) {
            byte[] bbuf = {(byte) i};
            boolean ok = good.asPredicate().test(new String(bbuf));
            legal[i] = ok;
        }
    }

    private static <T> T croak(String s, Object... x) {
        System.err.printf(s + "\n", x);
        System.exit(1);
        return null;
    }

    public static NGramLoader getDefault() {
        return selectByKey("oanc");
    }

    /**
     * Meant for UI/command line.
     *
     * @return
     */
    public static String[] listOptions() {
        String[] s = new String[opts.length];
        for (int i = 0; i < opts.length; i++) {
            s[i] = opts[i].key() + "\t\t" + opts[i].describe();
        }
        Arrays.sort(s);
        return s;
    }

    public static NGramLoader selectByKey(String key) {
        for (NGramLoader l : opts) {
            if (l.key().equals(key)) {
                return l;
            }
        }
        return null;
    }

    public static boolean loadByKey(String key, boolean verbose) {
        NGramLoader l = selectByKey(key);
        if (l == null) {
            return false;
        }
        l.load(wordLookup, intLookup, unigrams, bigrams, trigrams, quatrigrams);
        if (verbose) {
            commentOnNGrams(wordLookup, unigrams, bigrams, trigrams, quatrigrams);
        }
        return true;
    }

    /**
     * Takes a word, and returns its first reduction as a byte string. {That is,
     * all numbers are replaced by 0; `` becomes ", and '' becomes ".}
     *
     * This should significantly reduce memory and increase lookup space.
     *
     * @param s
     * @return
     */
    public static byte[] downCastWord(String s, boolean stdin) {
        if (stdin) {
            // The corpora are reasonably well normalized w.r.t punctuation
            if (s.contains("``") || s.contains("''")) {
                s = s.replace("''", "\"");
                s = s.replace("``", "\"");
            }
        }
        // Convert and then downcast numbers
        byte[] data = s.getBytes(StandardCharsets.US_ASCII);
        downCastBWord(data);
        return data;
    }

    public static void downCastBWord(byte[] data) {
        for (int i = 0; i < data.length; i++) {
            if (data[i] >= (byte) '0' && data[i] <= (byte) '9') {
                data[i] = (byte) '0';
            }
        }
    }

    private static InputStream corpusFileStream(String name) {
        String n = Util.DATA_PATH + name + ".gz";
        InputStream s;
        try {
            s = new GZIPInputStream(new FileInputStream(n), 1 << 16);
        } catch (IOException ex) {
            return croak("Could not open corpoid (%s)", n);
        }
        System.err.printf("Reading %s ...\n", n);
        return s;
    }

    private static void commentOnNGrams(List<byte[]> wordLookup, NGramTable unigrams, NGramTable bigrams,
            NGramTable trigrams, NGramTable quatrigrams) {
        System.out.printf("Unigram count: %d\n", unigrams.size());
        System.out.printf("Bigram count: %d\n", bigrams.size());
        System.out.printf("Trigram count: %d\n", trigrams.size());
        System.out.printf("Quatrigram count: %d\n", quatrigrams.size());
        System.out.printf("Total words: %d\n", wordLookup.size());
    }

    private static int loadWord(byte[] data, MapBytesToInt intLookup, List<byte[]> wordLookup) {
        downCastBWord(data);
        int k = intLookup.getOrDefault(data, -1);
        if (k == -1) {
            k = wordLookup.size() + Util.N_RESERVED_IDS;
            intLookup.put(data, k);
            wordLookup.add(data);
        }
        return k;
    }

    /**
     * I expect _strict_ formatting; then they load faster.
     *
     * WORD0\tWORD1\tWORD2..\n
     *
     * This method does not distinguish between ints/bytes; the byte[]->count
     * conversion is one you can do later. Yes, this thing spams temporaries.
     */
    private static void readTSVData(InputStream istream, MapBytesToInt intLookup, List<byte[]> wordLookup, NGramTable t) {
        int size = 1 << 20;
        byte[] buf = new byte[size];
        try {
            int upperlimit = istream.read(buf);
            int[] markers = new int[6];
            int[] codes = new int[4];
            Arrays.fill(codes, -1);
            int eol = -1;
            while (true) {
                // Scan ahead to newline; then pull in int, N words
                eol++;
                int fnd = 1;
                int sol = eol;
                markers[0] = sol;
                while (buf[eol] != '\n' && eol < upperlimit) {
                    if (buf[eol] == '\t') {
                        markers[fnd] = eol;
                        fnd++;
                    }
                    eol++;
                }
                markers[fnd] = eol;
                fnd++;
                if (eol >= upperlimit) {
                    // Copy tail, read more, retry
                    int leftover = upperlimit - sol;
                    System.arraycopy(buf, sol, buf, 0, leftover);
                    int n = istream.read(buf, leftover, buf.length - leftover);
                    if (n < 0) {
                        break;
                    }
                    upperlimit = n + leftover;
                    eol = -1;
                    continue;
                }

                // TODO: integer conversion microroutine
                int count = Integer.parseInt(new String(buf, markers[0], markers[1] - markers[0], StandardCharsets.US_ASCII));

                for (int i = 1; i < fnd - 1; i++) {
                    byte[] s = Arrays.copyOfRange(buf, markers[i] + 1, markers[i + 1]);
                    int k = loadWord(s, intLookup, wordLookup);
                    codes[i - 1] = k;
                }
                // We assume it was empty before
                switch (t.getN()) {
                    case 1:
                        t.put(codes[0], count);
                        break;
                    case 2:
                        t.put(codes[0], codes[1], count);
                        break;
                    case 3:
                        t.put(codes[0], codes[1], codes[2], count);
                        break;
                    case 4:
                        t.put(codes[0], codes[1], codes[2], codes[3], count);
                        break;
                }
            }
        } catch (IOException ex) {
            croak("Hit in the IO. Ugh.");
        }
    }

    private static void countSentence(int[] a, NGramTable unigrams,
            NGramTable bigrams, NGramTable trigrams, NGramTable quatrigrams) {
        unigrams.increment(a[0]);
        if (a.length <= 1) {
            return;
        }
        unigrams.increment(a[1]);
        bigrams.increment(a[0], a[1]);
        if (a.length <= 2) {
            return;
        }
        unigrams.increment(a[2]);
        bigrams.increment(a[1], a[2]);
        trigrams.increment(a[0], a[1], a[2]);
        for (int i = 3; i < a.length; i++) {
            unigrams.increment(a[i]);
            bigrams.increment(a[i - 1], a[i]);
            trigrams.increment(a[i - 2], a[i - 1], a[i]);
            quatrigrams.increment(a[i - 3], a[i - 2], a[i - 1], a[i]);
        }
    }

    /**
     * Reads and simultaneously analyzes a corpus.
     *
     * (Why simultaneously? to minimize memory usage.)
     *
     * @param istream
     * @param wordLookup
     * @param intLookup
     */
    private static void fastMunchies(InputStream istream,
            List<byte[]> wordLookup, MapBytesToInt intLookup, NGramTable unigrams, NGramTable bigrams, NGramTable trigrams, NGramTable quatrigrams) {
        int size = 1 << 20;
        byte[] buf = new byte[size];
        int maxsent = 1 << 16;
        int[] sent = new int[maxsent];
        int nwords = 0;
        int total_words = 0;
        int total_sentences = 0;
        try {
            int upperlimit = istream.read(buf);

            int markbot = 0;
            int marktop = 0;
            while (true) {
                // Scan the next word, refilling if necessary
                markbot = marktop;
                while ((!legal[buf[markbot] & 0xFF]) && markbot < upperlimit) {
                    markbot++;
                }
                if (markbot >= upperlimit) {
                    // Not inside a word, so just reload the full buffer
                    upperlimit = istream.read(buf);
                    if (upperlimit < 0) {
                        break;
                    }
                    marktop = 0;
                    continue;
                }
                marktop = markbot;
                while (legal[buf[marktop] & 0xFF] && marktop < upperlimit) {
                    marktop++;
                }
                if (marktop >= upperlimit) {
                    // Inside a word, so restart from the beginning
                    int leftover = upperlimit - markbot;
                    System.arraycopy(buf, markbot, buf, 0, leftover);
                    int n = istream.read(buf, leftover, buf.length - leftover);
                    if (n < 0) {
                        // famous last words
                        break;
                    }
                    upperlimit = n + leftover;
                    marktop = 0;
                    continue;
                }

                // Now that we have a string, load it!
                byte[] s = Arrays.copyOfRange(buf, markbot, marktop);

                int k = loadWord(s, intLookup, wordLookup);
                sent[nwords] = k;
                nwords++;

                if (Util.isTerminal(s) || nwords >= maxsent) {
                    /* Pad blob with start/end markers & analyze */
                    int[] sbpc = new int[nwords + 2];
                    sbpc[0] = Util.HEAD_ID;
                    System.arraycopy(sent, 0, sbpc, 1, nwords);
                    sbpc[nwords + 1] = Util.TAIL_ID;
                    /* Analyze sentence */
                    countSentence(sbpc, unigrams, bigrams, trigrams, quatrigrams);
                    total_words += nwords;
                    total_sentences += 1;
                    nwords = 0;
                }
            }
        } catch (IOException ex) {
            croak("Hit in the IO. Ugh.");
        }
        System.out.printf("Sentences: %d\n", total_sentences);
        System.out.printf("Total words: %d\n", total_words);
    }

    private static void dumpData(String key, List<byte[]> wordLookup,
            NGramTable unigrams, NGramTable bigrams, NGramTable trigrams,
            NGramTable quatrigrams) {
        if (!Files.exists(Paths.get(Util.DATA_PATH + "dump/"))) {
            try {
                Files.createDirectories(Paths.get(Util.DATA_PATH + "dump/"));
            } catch (IOException ex) {
                croak("Failed to create directory %s for cache files", Util.DATA_PATH + "dump/");
            }
        }
        try {
            OutputStream ostream = new BufferedOutputStream(new FileOutputStream(Util.DATA_PATH + "dump/" + key + ".words"));
            for (byte[] b : wordLookup) {
                ostream.write(b);
                ostream.write('\n');
            }
            ostream.close();
            System.err.println("Wrote words");
            NGramTable.writeToFile(new FileOutputStream(Util.DATA_PATH + "dump/" + key + "4.gz"), quatrigrams);
            System.err.println("Wrote quatrigrams");
            NGramTable.writeToFile(new FileOutputStream(Util.DATA_PATH + "dump/" + key + "1.gz"), unigrams);
            System.err.println("Wrote unigrams");
            NGramTable.writeToFile(new FileOutputStream(Util.DATA_PATH + "dump/" + key + "2.gz"), bigrams);
            System.err.println("Wrote bigrams");
            NGramTable.writeToFile(new FileOutputStream(Util.DATA_PATH + "dump/" + key + "3.gz"), trigrams);
            System.err.println("Wrote trigrams");

        } catch (IOException ex) {
            croak("Can't write. Oh no!");
        }

    }

    private static void undumpData(String key, List<byte[]> wordLookup,
            MapBytesToInt intLookup, NGramTable unigrams, NGramTable bigrams,
            NGramTable trigrams, NGramTable quatrigrams) {
        try {
            // A sign you've been programming for too long at a stretch:
            // Rolling everything yourself, for the third time
            InputStream istream = new FileInputStream(Util.DATA_PATH + "dump/" + key + ".words");
            byte[] buf = new byte[1 << 16];
            int start;
            int end = 0;
            int limit = istream.read(buf, 0, buf.length);
            do {
                start = end;
                while (end < limit && buf[end] != '\n') {
                    end++;
                }
                if (end >= limit) {
                    int extra = limit - start;
                    System.arraycopy(buf, start, buf, 0, extra);
                    int n = istream.read(buf, extra, buf.length - extra);
                    if (n < 0) {
                        break;
                    }
                    limit = extra + n;
                    end = 0;
                    continue;
                }
                byte[] word = Arrays.copyOfRange(buf, start, end);
                intLookup.put(word, wordLookup.size() + Util.N_RESERVED_IDS);
                wordLookup.add(word);
                end++;
            } while (true);

            // Load in reverse order to avoid large memory jumps at the end
            NGramTable.fillFromFile(new FileInputStream(Util.DATA_PATH + "dump/" + key + "4.gz"), quatrigrams);
            NGramTable.fillFromFile(new FileInputStream(Util.DATA_PATH + "dump/" + key + "3.gz"), trigrams);
            NGramTable.fillFromFile(new FileInputStream(Util.DATA_PATH + "dump/" + key + "2.gz"), bigrams);
            NGramTable.fillFromFile(new FileInputStream(Util.DATA_PATH + "dump/" + key + "1.gz"), unigrams);

        } catch (IOException ex) {
            croak("Can't read. Oh no!");

        }
    }

    private static void munchTaggedSentence(InputStream istream,
            List<byte[]> wordLookup, MapBytesToInt intLookup,
            NGramTable unigrams, NGramTable bigrams, NGramTable trigrams,
            NGramTable quatrigrams, int nelements) {
        try {
            int nwords = 0;
            // Note: some sentences up to 80
            byte[] buf = new byte[1 << 20];
            buf[0] = (byte) ' ';
            int limit = istream.read(buf, 1, buf.length - 1) + 1;
            int eos = 0;
            while (nwords < nelements || nelements < 0) {
                // Ensure that we have a sentence between sos/eos
                int sos = eos;
                // ? sentinel trick?
                while (eos < limit && (char) buf[eos] != '\n') {
                    eos++;
                }
                if (eos >= limit) {
                    int extra = limit - sos;
                    System.arraycopy(buf, sos, buf, 0, extra);
                    int n = istream.read(buf, extra, buf.length - extra);
                    if (n < 0) {
                        break;
                    }
                    limit = extra + n;
                    eos = 0;
                    continue;
                }
                // Now, scan through the words.
                int[] lastwrd = new int[]{Util.HEAD_ID, Util.HEAD_ID,
                    Util.HEAD_ID, Util.HEAD_ID};
                // Sentinel!!
                buf[eos] = (byte) ' ';
                int eow = sos;
                int wps = 0;
                while (eow < eos) {
                    // Scan in words (forward), and ignore POS tag
                    int sow = eow;
                    while (buf[eow] != (byte) ' ') {
                        eow++;
                    }
                    if (eow == sow) {
                        // Skip dummy spaces
                        eow++;
                        continue;
                    }

                    int mow = eow;
                    while (buf[mow] != (byte) '_' && mow > sow) {
                        mow--;
                    }
                    if (mow == sow) {
                        // Word doesn't have a tag, for some reason.
                        mow = eow;
                    } else {
                        // No change
                    }

                    byte[] wrd = Arrays.copyOfRange(buf, sow, mow);
//                    System.out.printf("|%s|\n", new String(wrd));
                    int id = loadWord(wrd, intLookup, wordLookup);
                    wps++;

                    // rotate prev words out
                    lastwrd[0] = lastwrd[1];
                    lastwrd[1] = lastwrd[2];
                    lastwrd[2] = lastwrd[3];
                    lastwrd[3] = id;

                    // Thankfully we're not using the 5^k
                    // Wildcard, POS, Lemma, Word, TaggedWord
                    // backoff chains
                    unigrams.increment(lastwrd[3]);
                    if (wps >= 2) {
                        bigrams.increment(lastwrd[2], lastwrd[3]);
                    }
                    if (wps >= 3) {
                        trigrams.increment(lastwrd[1], lastwrd[2], lastwrd[3]);
                    }
                    if (wps >= 4) {
                        quatrigrams.increment(lastwrd[0], lastwrd[1], lastwrd[2], lastwrd[3]);
                    }
                }
                lastwrd[0] = lastwrd[1];
                lastwrd[1] = lastwrd[2];
                lastwrd[2] = lastwrd[3];
                lastwrd[3] = Util.TAIL_ID;
                if (wps >= 1) {
                    bigrams.increment(lastwrd[2], lastwrd[3]);
                }
                if (wps >= 2) {
                    trigrams.increment(lastwrd[1], lastwrd[2], lastwrd[3]);
                }
                if (wps >= 3) {
                    quatrigrams.increment(lastwrd[0], lastwrd[1], lastwrd[2], lastwrd[3]);
                }
                nwords += wps;
            }
        } catch (IOException ex) {
            croak("Can't read. Oh no!");
        }
    }

    public static interface NGramLoader {

        String describe();

        String key();

        boolean canLoad();

        void load(List<byte[]> wordLookup,
                MapBytesToInt intLookup, NGramTable unigrams, NGramTable bigrams,
                NGramTable trigrams, NGramTable quatrigrams);
    }

    private static class CorpusNGramSource implements NGramLoader {

        private final String key;
        private final String desc;

        public CorpusNGramSource(String key, String desc) {
            this.key = key;
            this.desc = desc;
        }

        @Override
        public String describe() {
            return "Corpus source: " + desc;
        }

        @Override
        public boolean canLoad() {
            boolean cex = Files.exists(Paths.get(Util.DATA_PATH + key + ".gz"));

            return cex;
        }

        @Override
        public void load(List<byte[]> wordLookup, MapBytesToInt intLookup, NGramTable unigrams, NGramTable bigrams, NGramTable trigrams, NGramTable quatrigrams) {
            fastMunchies(corpusFileStream(key), wordLookup, intLookup, unigrams, bigrams, trigrams, quatrigrams);
        }

        @Override
        public String key() {
            return key;
        }
    }

    private static class TableNGramSource implements NGramLoader {

        private final String key;
        private final String desc;

        public TableNGramSource(String key, String desc) {
            this.key = key;
            this.desc = desc;
        }

        @Override
        public String describe() {
            return "Table source: " + desc;
        }

        @Override
        public boolean canLoad() {
            boolean uex = Files.exists(Paths.get(Util.DATA_PATH + key + "1.gz"));
            boolean bex = Files.exists(Paths.get(Util.DATA_PATH + key + "2.gz"));
            boolean tex = Files.exists(Paths.get(Util.DATA_PATH + key + "3.gz"));
            boolean qex = Files.exists(Paths.get(Util.DATA_PATH + key + "4.gz"));
            return uex && bex && tex && qex;
        }

        @Override
        public void load(List<byte[]> wordLookup, MapBytesToInt intLookup, NGramTable unigrams, NGramTable bigrams, NGramTable trigrams, NGramTable quatrigrams) {
            // Note that words in one list need not appear in another.
            readTSVData(corpusFileStream(key + "1"), intLookup, wordLookup, unigrams);
            readTSVData(corpusFileStream(key + "2"), intLookup, wordLookup, bigrams);
            readTSVData(corpusFileStream(key + "3"), intLookup, wordLookup, trigrams);
            readTSVData(corpusFileStream(key + "4"), intLookup, wordLookup, quatrigrams);
        }

        @Override
        public String key() {
            return key;
        }
    }

    private static class DumpNGramSource implements NGramLoader {

        private final String key;
        private final String desc;

        public DumpNGramSource(NGramLoader l) {
            this.key = l.key();
            this.desc = l.describe();
        }

        @Override
        public String describe() {
            return "Preprocessed " + desc;
        }

        @Override
        public boolean canLoad() {
            boolean wex = Files.exists(Paths.get(Util.DATA_PATH + "dump/" + key + ".words"));
            boolean uex = Files.exists(Paths.get(Util.DATA_PATH + "dump/" + key + "1.gz"));
            boolean bex = Files.exists(Paths.get(Util.DATA_PATH + "dump/" + key + "2.gz"));
            boolean tex = Files.exists(Paths.get(Util.DATA_PATH + "dump/" + key + "3.gz"));
            boolean qex = Files.exists(Paths.get(Util.DATA_PATH + "dump/" + key + "4.gz"));
            return wex && uex && bex && tex && qex;
        }

        @Override
        public void load(List<byte[]> wordLookup, MapBytesToInt intLookup, NGramTable unigrams, NGramTable bigrams, NGramTable trigrams, NGramTable quatrigrams) {
            // Note that words in one list need not appear in another.
            undumpData(key, wordLookup, intLookup, unigrams, bigrams, trigrams, quatrigrams);
        }

        @Override
        public String key() {
            return key;
        }
    }

    private static class TaggedCorpusSource implements NGramLoader {

        private final String key;
        private final String desc;
        private final String file;
        private final int nelements;

        public TaggedCorpusSource(String key, String desc) {
            this.key = key;
            this.file = key;
            this.desc = desc;
            this.nelements = -1;
        }

        public TaggedCorpusSource(String key, String alt, String desc, int nelements) {
            this.file = key;
            this.key = alt;
            this.desc = desc;
            this.nelements = nelements;
        }

        @Override
        public String describe() {
            return "Tagged corpus source: " + desc;
        }

        @Override
        public String key() {
            return key;
        }

        @Override
        public boolean canLoad() {
            return Files.exists(Paths.get(Util.DATA_PATH + file + ".gz"));
        }

        @Override
        public void load(List<byte[]> wordLookup, MapBytesToInt intLookup, NGramTable unigrams, NGramTable bigrams, NGramTable trigrams, NGramTable quatrigrams) {
            munchTaggedSentence(corpusFileStream(file), wordLookup, intLookup, unigrams, bigrams, trigrams, quatrigrams, nelements);
        }
    }

    private static class CachedNGramSource implements NGramLoader {

        private NGramLoader primary;
        private NGramLoader cache;

        CachedNGramSource(NGramLoader given) {
            primary = given;
            cache = new DumpNGramSource(given);
        }

        @Override
        public String describe() {
            return "Cached " + primary.describe();
        }

        @Override
        public String key() {
            return primary.key();
        }

        @Override
        public boolean canLoad() {
            return cache.canLoad() || primary.canLoad();
        }

        @Override
        public void load(List<byte[]> wordLookup, MapBytesToInt intLookup, NGramTable unigrams, NGramTable bigrams, NGramTable trigrams, NGramTable quatrigrams) {
            if (cache.canLoad()) {
                cache.load(wordLookup, intLookup, unigrams, bigrams, trigrams, quatrigrams);
            } else {
                unigrams.setEnabled(true);
                bigrams.setEnabled(true);
                trigrams.setEnabled(true);
                quatrigrams.setEnabled(true);
                System.err.println("Loading, not from cache.");
                primary.load(wordLookup, intLookup, unigrams, bigrams, trigrams, quatrigrams);
                System.err.println("Loaded. Now dumping.");
                dumpData(primary.key(), wordLookup, unigrams, bigrams, trigrams, quatrigrams);
                System.err.println("Dumping complete.");
            }
        }
    }

    private static class FinalNGramSource implements NGramLoader {

        private final String key;
        private final String desc;

        public FinalNGramSource(String tagfinal, String desc) {
            this.desc = desc;
            this.key = tagfinal;
        }

        @Override
        public boolean canLoad() {
            return Files.exists(Paths.get(Util.DATA_PATH + "tagbnc.gz"))
                    && Files.exists(Paths.get(Util.DATA_PATH + "tagoanc.gz"))
                    && Files.exists(Paths.get(Util.DATA_PATH + "wikipedia.gz"))
                    && Files.exists(Paths.get(Util.DATA_PATH + "aclarc.gz"));
        }

        @Override
        public void load(List<byte[]> wordLookup, MapBytesToInt intLookup, NGramTable unigrams, NGramTable bigrams, NGramTable trigrams, NGramTable quatrigrams) {
            // For technical stuff
            fastMunchies(corpusFileStream("aclarc"), wordLookup, intLookup, unigrams, bigrams, trigrams, quatrigrams);
            // Alltagsgebrauch
            munchTaggedSentence(corpusFileStream("tagoanc"), wordLookup, intLookup, unigrams, bigrams, trigrams, quatrigrams, -1);
            munchTaggedSentence(corpusFileStream("tagbnc"), wordLookup, intLookup, unigrams, bigrams, trigrams, quatrigrams, -1);
            // For proper nouns
            munchTaggedSentence(corpusFileStream("wikipedia"), wordLookup, intLookup, unigrams, bigrams, trigrams, quatrigrams, -1);
        }

        @Override
        public String describe() {
            return desc;
        }

        @Override
        public String key() {
            return key;
        }
    }

    private static class SemiFinalNGramSource implements NGramLoader {

        private final String key;
        private final String desc;

        public SemiFinalNGramSource(String tagfinal, String desc) {
            this.desc = desc;
            this.key = tagfinal;
        }

        @Override
        public boolean canLoad() {
            return Files.exists(Paths.get(Util.DATA_PATH + "tagbnc.gz"))
                    && Files.exists(Paths.get(Util.DATA_PATH + "tagoanc.gz"));
        }

        @Override
        public void load(List<byte[]> wordLookup, MapBytesToInt intLookup, NGramTable unigrams, NGramTable bigrams, NGramTable trigrams, NGramTable quatrigrams) {
            munchTaggedSentence(corpusFileStream("tagoanc"), wordLookup, intLookup, unigrams, bigrams, trigrams, quatrigrams, -1);
            munchTaggedSentence(corpusFileStream("tagbnc"), wordLookup, intLookup, unigrams, bigrams, trigrams, quatrigrams, -1);
        }

        @Override
        public String describe() {
            return desc;
        }

        @Override
        public String key() {
            return key;
        }
    }
}
