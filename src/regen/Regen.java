package regen;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;

/**
 *
 * @author msto
 */
public class Regen {

    /* Each of these recreated for a sentence */
    public static interface Filter {

        /**
         * Rank a sentence. Higher is better. Dummy is true if the sentence's
         * handling should have no impact on future behavior.
         */
        double score(String sentence, boolean dummy);

        default void setSource(String original) {
        }

        default void runSetup() {
        }

        String shortName();

    }

    public static interface Generator {

        /* Feed it the given phrase from which it generates */
        void setSource(String original);

        /* Returning null => Giving up */
        String next();

        default void runSetup() {
        }
    }

    /**
     * Generate sequences at random. Not recommended for under-1K tasks.
     */
    public static class RandomGenerator implements Generator {

        private String[] words;

        RandomGenerator() {
            words = null;
        }

        @Override
        public String next() {
            String[] scramble = Util.shuffledOf(words);
            return String.join(" ", scramble);
        }

        @Override
        public void setSource(String given) {
            words = given.split(" ");
        }
    }

    /**
     * Trivial filter, just in case
     */
    public static class TerminatorFilter implements Filter {

        @Override
        public double score(String sent, boolean dummy) {
            int i = Util.isInitial(sent) ? 1 : 0;
            int j = Util.isTerminal(sent) ? 1 : 0;
            return 1.0 * i + 0.9 * j;
        }

        @Override
        public String shortName() {
            return "Sentence termination";
        }

    }

    public static class MatchedParenFilter implements Filter {

        @Override
        public double score(String sentence, boolean dummy) {
            // note: Need emoticon avoidance
            char[] c = sentence.toCharArray();
            int depth = 0;
            for (char d : c) {
                if (d == ')') {
                    depth--;
                } else if (d == '(') {
                    depth++;
                }
                if (depth < 0) {
                    // BAD!
                    return 0.0;
                }
            }
            // GOOD!
            return 1.0;
        }

        @Override
        public String shortName() {
            return "Matched parenthesis";
        }
    }

    public static class HygieneFilter implements Filter {

        @Override
        public void runSetup() {
            NGramSource.unigrams.setEnabled(true);
        }

        private Map<String, Double> capitalFreq;
        private char[] stack = new char[256];

        public void setSource(String orig) {
            String[] parts = orig.split(" ");
            // For speed, _ought_ to just make a perfect hash function
            capitalFreq = new HashMap<>(parts.length * 3);
            double minscore = 1.0;
            for (String s : parts) {
                byte[] capform = NGramSource.downCastWord(s, true);
                int capid = NGramSource.intLookup.getOrDefault(capform, -1);
                int ccount = NGramSource.unigrams.getOrDefault(capid, 0) + 1;
                String lower = s.toLowerCase(Locale.ENGLISH);
                byte[] subform = NGramSource.downCastWord(lower, true);
                int subid = NGramSource.intLookup.getOrDefault(subform, -1);
                int scount = NGramSource.unigrams.getOrDefault(subid, 0) + 1;
                boolean isnumeric = false;
                for (byte b : capform) {
                    if (b == (byte) '0') {
                        isnumeric = true;
                    }
                }

                double score;
                if (isnumeric) {
                    // Numbers go later
                    score = -1.0;
                } else if (lower.equals(s)) {
                    // ^ I.e, no case folding occurred. Technically only
                    // the leading capital matters, but in the long run, whatever.
                    score = 0.0;
                } else {
                    score = (double) (ccount) / (double) (scount + ccount);
                    minscore = Math.min(score, minscore);
                }

                capitalFreq.put(s, score);
            }
            for (String s : parts) {
                double d = capitalFreq.get(s);
                if (d < 0) {
                    // Numeric penalty
                    capitalFreq.put(s, Math.min(1.0, minscore * 1.2));
                }
            }
        }

        @Override
        public double score(String sentence, boolean dummy) {
            String[] splitsent = sentence.split(" ");
            double bbscore = getBracketScore(sentence);
            double properscore = getProperScore(splitsent);
            double doublescore = getDoubledWordScore(splitsent);
            // Add a bit of noise to avoid filtering out all viable sentences
            double score = bbscore + 1.5 * properscore + doublescore * 10.0 + 0.25 * Util.RAND.nextDouble();
            return score;
        }

        @Override
        public String shortName() {
            return "Hygiene filter";
        }

        private double getBracketScore(String sentence) {
            // TODO: Double quote matching!
            int nbrackets = 0;
            int nviolations = 0;
            int depth = 0;
            for (int i = 0; i < sentence.length() && depth < stack.length - 2; i++) {
                char c = sentence.charAt(i);
                switch (c) {
                    case '(':
                    case '[':
                    case '{':
                        //case '"':// special: depends on previous space/char.
                        // '("ABC is accept;
                        stack[depth] = c;
                        depth++;
                        nbrackets++;
                        break;
                    case ')':
                    case ']':
                    case '}':
                        if (depth <= 0) {
                            nviolations++;
                        } else {
                            depth--;
                            // Check the character
                            if (c == ')' && stack[depth] == '(') {
                            } else if (c == ']' && stack[depth] == '[') {
                            } else if (c == '}' && stack[depth] == '{') {
                            } else {
                                nviolations++;
                            }
                        }
                        nbrackets++;
                        break;

                    default:
                        break;
                }
            }

            if (nbrackets == 0) {
                return 1.0;
            }

            double score = 1.0 - (double) nviolations / (double) nbrackets;
            return score;
        }

        private double getProperScore(String[] words) {
            // Score by the unlikelyhood of having
            // the sequence of capital words be it.
            double rscore = 0;
            int nlead = 0;
            for (String sub : words) {
                double cp = capitalFreq.get(sub);
                if (cp == 0) {
                    break;
                }
                rscore += (1.0 - cp);
                nlead++;
            }

            if (nlead == 0) {
                return 1.0;
            }

            double score = rscore / nlead;

            return score;
        }

        private static final Set<String> doubled = new HashSet<>(50);

        {
            // Things that get doubled but _never_ should be
            doubled.add("I");
            doubled.add("the");
        }

        private double getDoubledWordScore(String[] splitsent) {
            // 70% penalty for each doubled unnatural word word.
            double rm = 1.0;
            for (int i = 0; i < splitsent.length - 1; i++) {
                if (splitsent[i].equals(splitsent[i + 1])) {
//                    System.err.printf("Doubled %f %s %s\n", rm * 0.3, splitsent[i], Arrays.toString(splitsent));
                    rm *= 0.3;
                }
            }
            return rm;
        }
    }

    public static class RandomFilter implements Filter {

        @Override
        public double score(String sentence, boolean dummy) {
            return Util.RAND.nextDouble();
        }

        @Override
        public String shortName() {
            return "Random";
        }
    }

    public static class DuplicateFilter implements Filter {

        // TODO: better, space-savinger collection
        private Set<String> seen = null;

        @Override
        public double score(String sentence, boolean dummy) {
            if (dummy) {
                return 1.0;
            }
            boolean novel = seen.add(sentence);
            return novel ? 1.0 : 0.0;
        }

        public void setSource(String src) {
            seen = new HashSet<String>();
        }

        @Override
        public String shortName() {
            return "Duplicates";
        }

    }

    public static class Pipeline {

        public Pipeline(String desc) {
            this.desc = desc;
        }

        public final String desc;
        public Filter[] filters;
        public double[] passrates;
        public Generator source;
        public Filter ranker;
    }

    private static Pipeline[] pipelines = {
        /*
         * Warning! Although a duplicate filter sounds nice, because of
         * the variation in alpha levels, future stages are never
         * deterministic -- a duplicate filter placed too early can
         * permanently eliminate valid options. (Then again, so can
         * a bad alpha-value that doesn't take identity into account.)
         */
        // TODO: Specify filters on the command line. local => ngram 0.6 => paren 0.3 => duplicate 0.3 => parse
        new Pipeline(
        "local-hyg-ngram") {
            {
                filters = new Filter[]{new HygieneFilter()};
                passrates = new double[]{0.5};
                source = new NGramLocalGenerator();
                ranker = new TrigramFilter();
            }
        },
        new Pipeline(
        "local-ngram") {
            {
                filters = new Filter[]{};
                passrates = new double[]{};
                source = new NGramLocalGenerator();
                ranker = new TrigramFilter();
            }
        },
        new Pipeline(
        "local-quadgram") {
            {
                filters = new Filter[]{};
                passrates = new double[]{};
                source = new NGramGenerator();
                ranker = new QuadgramFilter();
            }
        },
        new Pipeline(
        "local-ngram-quadgram") {
            {
                filters = new Filter[]{new TrigramFilter()};
                passrates = new double[]{0.05};
                source = new NGramGenerator();
                ranker = new QuadgramFilter();
            }
        },
        new Pipeline("ngram-ngram-nlp") {
            {
                filters = new Filter[]{
                    new TrigramFilter(),
                    new MatchedParenFilter(),
                    new DuplicateFilter()
                };
                source = new NGramGenerator();
                passrates = new double[]{0.5, 0.5, 0.05};
                ranker = new ParseFilter();
            }
        },
        new Pipeline("local-ngram-nlp") {
            {
                filters = new Filter[]{
                    new TrigramFilter(),
                    new MatchedParenFilter(),
                    new DuplicateFilter()
                };
                source = new NGramLocalGenerator();
                passrates = new double[]{0.5, 0.5, 0.05};
                ranker = new ParseFilter();
            }
        },
        new Pipeline("local-ngram-srp") {
            {
                filters = new Filter[]{
                    new TrigramFilter(),
                    new MatchedParenFilter(),
                    new DuplicateFilter()
                };
                source = new NGramLocalGenerator();
                passrates = new double[]{0.5, 0.5, 0.05};
                ranker = new ParseSRFilter();
            }
        },
        new Pipeline("local-ngram-srmle") {
            {
                filters = new Filter[]{
                    new TrigramFilter(),
                    new MatchedParenFilter(),
                    new DuplicateFilter()
                };
                source = new NGramLocalGenerator();
                passrates = new double[]{0.5, 0.5, 0.05};
                ranker = new ParseSRMLEFilter();
            }
        },
        new Pipeline("rand-ngram-nlp") {
            {
                filters = new Filter[]{
                    new TerminatorFilter(),
                    new TrigramFilter(),
                    new MatchedParenFilter(),
                    new DuplicateFilter()};
                source = new RandomGenerator();
                // Make passrates a filter attribute?
                // Some _are_ binary (read -- won't allow bad results if
                // a single good one comes through )
                passrates = new double[]{0.05, 0.05, 0.05, 0.5};
                ranker = new ParseFilter();
            }
        },
        new Pipeline(
        "ngram-ngram") {
            {
                filters = new Filter[]{};
                passrates = new double[]{};
                source = new NGramGenerator();
                ranker = new TrigramFilter();
            }
        },
        new Pipeline(
        "rand-ngram") {
            {
                filters = new Filter[]{
                    new TerminatorFilter(),
                    new MatchedParenFilter()
                };
                source = new RandomGenerator();
                passrates = new double[]{0.05, 0.05};
                ranker = new TrigramFilter();
            }
        },
        new Pipeline(
        "rand-rand") {
            {
                filters = new Filter[]{};
                source = new RandomGenerator();
                passrates = new double[]{};
                ranker = new RandomFilter();
            }
        }
    };

    private static void usage() {
        System.err.println("Usage: regen pipeline {ngram-source} {time/task} {testbank}");
        System.err.println("Or:    regen < ta.input > ta.output");
        System.err.println("Or:    regen serve {pipeline, etc...}");
        System.err.println();
        System.err.println("Pipelines");
        for (Pipeline p : pipelines) {
            System.err.format("   %s    %s\n", p.desc, "Insert explanation here.");
        }

        System.err.println();
        System.err.println("The available data sources are:");
        String[] opts = NGramSource.listOptions();
        for (String o : opts) {
            System.err.format("\t%s\n", o);
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // We determine boring_mode by # of args, and pipe presence
        boolean boring_mode = false;
        if (args.length == 0) {
            if (System.console() == null) {
                boring_mode = true;
                System.err.println("Boring mode activated!");
            }
        } else if (args.length >= 1 && args[0].equals("serve")) {
            System.err.println("Server mode. (Okay, still boring mode.)");
            boring_mode = true;
            args = Arrays.copyOfRange(args, 1, args.length);
        }
        if (!boring_mode) {
            System.out.println(Arrays.toString(args));
        }

        Pipeline pipeline = null;
        if (args.length >= 1) {
            for (Pipeline p : pipelines) {
                if (p.desc.equals(args[0])) {
                    pipeline = p;
                }
            }
            if (pipeline == null) {
                usage();
                return;
            }
        } else {
            pipeline = pipelines[0];
        }

        // Save memory for unused parts. NOTE: Make a system to reenable if
        // need be!
        NGramSource.unigrams.setEnabled(false);
        NGramSource.bigrams.setEnabled(false);
        NGramSource.trigrams.setEnabled(false);
        NGramSource.quatrigrams.setEnabled(false);

        // Run pipeline setup, enabling whatever ngrams are necessary
        // (and loading other resources)
        pipeline.ranker.runSetup();
        for (Filter f : pipeline.filters) {
            // NOTE: Could do n-gram requesting here
            f.runSetup();
        }

        if (args.length >= 2) {
            if (!NGramSource.loadByKey(args[1], true)) {
                usage();
                return;
            }
        } else {
            // Final submission mode: expensive!
            NGramSource.loadByKey("tagfinal", !boring_mode);
        }

        double timePerTask = 1.0;
        if (args.length >= 3) {
            try {
                timePerTask = Double.valueOf(args[2]);
            } catch (NumberFormatException e) {
                usage();
                return;
            }
            if (timePerTask < 0.001) {
                usage();
                return;
            }
        }
        String fname = "ta.test";
        if (args.length >= 4) {
            fname = args[3];
        }

        String color = "";
        String hicolor = "";
        String decolor = "";
        if (System.console() != null) {
            color = "\u001b[93m";
            hicolor = "\u001b[92m";
            decolor = "\u001b[0m";
        }

        // Boring mode
        if (boring_mode) {
            BufferedReader b = new BufferedReader(new InputStreamReader(System.in));
            String x;
            ArrayList<String> words = new ArrayList<>();
            do {
                try {
                    x = b.readLine();
                } catch (IOException ex) {
                    x = null;
                }
                if (x != null) {
                    if (x.contains("=====")) {
                        if (words.size() == 0) {
                            System.out.println("=====");
                            continue;
                        }
                        // This is the delimeter line.
                        String sent = String.join(" ", words);
                        words.clear();
                        // Need to gen. at least two, for some reason.
                        String[] q = regenerate(sent, pipeline, 2, timePerTask, false);
                        if (q.length < 1) {
                            // Give up!
                            System.out.println(sent);
                        } else {
                            System.out.println(q[q.length - 1]);
                        }
                        System.out.println("=====");
                    } else {
                        for (String s : x.split("[\r\n\t ]")) {
                            if (!s.isEmpty()) {
                                words.add(s);
                            }
                        }
                    }
                }
            } while (x != null);

            return;
        }

        // Init info
        System.out.printf("%sPipeline: %s ", color, pipeline.source.getClass().getCanonicalName());
        for (Filter f : pipeline.filters) {
            System.out.printf("-> %s ", f.shortName());
        }
        System.out.printf("-> %s%s\n", pipeline.ranker.shortName(), decolor);

        // Run!
        long itime = System.nanoTime();
        String[][] sents = Util.loadTestInput(Util.DATA_PATH + "/" + fname);
        ArrayList<String> chosen = new ArrayList();
        ArrayList<String> gold = new ArrayList();
        int correct = 0;
        for (int i = 0; i < sents.length; i++) {
            String[] s = sents[i];
            System.out.println();
            String clean = String.join(" ", s);
            System.out.println(clean);

            String[] q = regenerate(clean, pipeline, 3, timePerTask, true);
            for (int j = 0; j < q.length; j++) {
                String t = q[j];
                if (j == q.length - 1) {
                    System.out.format(" |%s%s%s|\n", t.equals(clean) ? hicolor : color, t, decolor);
                } else {
                    System.out.format(" |%s|\n", t);
                }
            }
            // Log for BLEU analysis
            String[] spl;
            if (q.length > 0) {
                spl = q[q.length - 1].split(" ");
                if (Arrays.deepEquals(spl, s)) {
                    correct++;
                }
            } else {
                String[] dup = Arrays.copyOf(s, s.length);
                Arrays.sort(dup);
                spl = dup;
            }
            for (String z : spl) {
                chosen.add(z);
            }
            chosen.add(null);
            for (String z : s) {
                gold.add(z);
            }
            gold.add(null);
        }
        long ftime = System.nanoTime();

        // Post info
        System.out.println();
        System.out.printf("Took %f secs/sentence; goal %f\n",
                (ftime - itime) * 1e-9 / sents.length, timePerTask);
        double bleu = Scoring.bleuScore(chosen.toArray(new String[0]), gold.toArray(new String[0]), true);
        double rate = (double) correct / (double) sents.length;
        System.out.printf("Entire document: BLEU score: %s%f%s Fraction correct: %s%f%s\n",
                color, bleu, decolor, color, rate, decolor);
    }

    private static String[] runIDPidpeline(String given, Pipeline pipeline, int N, double time_in_seconds) {
        // Semi-failed experiment -- good in theory, but harder to express
        // a single classifying behavior, and avoid bunching. Also would need
        // an mechanism to handle residual items (in pre-rank order) once
        // source pool is dry. Hm -- could rearchitect into a mode where
        // the ranker requests strings, and toolchain generates new ones and
        // either passes them into the reject pile (which, on passing a
        // threshold _value_ (not count), yields a new item.
        pipeline.source.setSource(given);
        for (Filter s : pipeline.filters) {
            s.setSource(given);
        }
        pipeline.ranker.setSource(given);
        int nfilts = pipeline.filters.length;

        int min_end_pool = 10;
        // Eventually, would need to compress/optimize these datastructures.
        // A specialized implementation _could_ save ~ 80% of space, 60% of time,
        // I'd guess.
        Set<String> idPool = new HashSet<>();
        TreeMap<Double, LinkedList<String>> finalBucket = new TreeMap<>();
        TreeMap<Double, LinkedList<String>> preFinalBucket = new TreeMap<>();
        TreeMap<Double, LinkedList<String>>[] filterBuckets = new TreeMap[nfilts];
        int[] stage_totals = new int[nfilts];
        int[] stage_exits = new int[nfilts];
        ArrayList<String>[] filterQueues = new ArrayList[nfilts];
        for (int i = 0; i < nfilts; i++) {
            filterBuckets[i] = new TreeMap<>();
            filterQueues[i] = new ArrayList<>();
        }

        long etime = System.nanoTime() + (long) (time_in_seconds * 1e9);
        int copycounter = 0;
        int prefinalcount = 0;
        int finalcount = 0;
        while (System.nanoTime() <= etime) {
            String poss = pipeline.source.next();
            boolean novel = idPool.add(poss);
            if (!novel) {
                copycounter++;
                if (copycounter > 2000) {
                    // Exhausted.
                    break;
                }
                continue;
            }
            copycounter = 0;

            if (nfilts > 0) {
                filterQueues[0].add(poss);
                for (int i = 0; i < nfilts; i++) {
                    for (String s : filterQueues[i]) {
                        // Rate, put in next bucket
                        double score = pipeline.filters[i].score(s, false);
                        stage_totals[i] += 1;
                        if (filterBuckets[i].containsKey(score)) {
                            filterBuckets[i].get(score).add(s);
                        } else {
                            LinkedList<String> l = new LinkedList<>();
                            l.add(s);
                            filterBuckets[i].put(score, l);
                        }
                    }
                    filterQueues[i].clear();

                    // Figure out which elements are now above the threshold, and
                    // bump them to the next level. (but wait -- doesn't this
                    // unfairly penalize quantized filters?) May need a flag.
                    // (also-min-count fail is weird)
                    while (stage_exits[i] <= pipeline.passrates[i] * stage_totals[i]) {
                        // Move the entirety of the next-highest bucket over.
                        double next_best = filterBuckets[i].lastKey();
                        LinkedList<String> set = filterBuckets[i].remove(next_best);
                        stage_exits[i] += set.size();
                        if (i != nfilts - 1) {
                            filterQueues[i + 1].addAll(set);
                        } else if (preFinalBucket.containsKey(next_best)) {
                            prefinalcount += set.size();
                            preFinalBucket.get(next_best).addAll(set);
                        } else {
                            prefinalcount += set.size();
                            preFinalBucket.put(next_best, set);
                        }
                    }
                    // Rate, place, move elements across buckets
                }
            } else if (preFinalBucket.containsKey(0.0)) {
                prefinalcount++;
                preFinalBucket.get(0.0).add(poss);
            } else {
                prefinalcount++;
                LinkedList<String> l = new LinkedList<>();
                l.add(poss);
                preFinalBucket.put(0.0, l);
            }

//            System.out.printf("Pipe: %d %d |", copycounter, idPool.size());
//            for (int i = 0; i < nfilts; i++) {
//                System.out.printf(" %d", stage_totals[i]);
//            }
//            System.out.printf(" F %d -> %d.\n", prefinalcount, finalcount);
            while (prefinalcount > min_end_pool && System.nanoTime() <= etime) {
                Entry<Double, LinkedList<String>> e = preFinalBucket.lastEntry();
                LinkedList<String> j = e.getValue();
                String target = j.remove();
                if (j.isEmpty()) {
                    preFinalBucket.remove(e.getKey());
                }
                prefinalcount--;
                finalcount++;
                // Apply ranker
                double score = pipeline.ranker.score(target, false);
                if (finalBucket.containsKey(score)) {
                    finalBucket.get(score).add(poss);
                } else {
                    LinkedList<String> l = new LinkedList<>();
                    l.add(poss);
                    finalBucket.put(score, l);
                }
                while (finalcount > N) {
                    // Discard lowest remaining values
                    Entry<Double, LinkedList<String>> e2 = finalBucket.firstEntry();
                    LinkedList<String> j2 = e2.getValue();
                    j2.remove();
                    if (j2.isEmpty()) {
                        finalBucket.remove(e2.getKey());
                    }
                    finalcount--;
                }
            }

            // Print bucket states for diagnostic purposes
            // Next filters, grab all left under threshold.
            // If _no_ filter, add to final pool with score _zero_.
            // Finally, check pool and put into ranker if > 50, then put in last pool.
        }
        Collection<LinkedList<String>> fn = finalBucket.values();
        ArrayList<String> s = new ArrayList<>();
        for (LinkedList<String> q : fn) {
            for (String j : q) {
                s.add(j);
            }
        }
        return s.toArray(new String[0]);

    }

    private static String[] regenerate(String given, Pipeline pipeline, int N, double time_in_seconds, boolean verbose) {
        // Initialize pipeline
        pipeline.source.setSource(given);
        for (Filter s : pipeline.filters) {
            s.setSource(given);
        }
        pipeline.ranker.setSource(given);
        int nfilts = pipeline.filters.length;

        // More filtering miscellany
        int[] counts = new int[nfilts];
        int[] winrate = new int[nfilts];
        double[] totals = new double[nfilts];
        double[] maxima = new double[nfilts];
        Arrays.fill(maxima, Double.NEGATIVE_INFINITY);
        double[] alphas = new double[nfilts];
        Arrays.fill(alphas, .5);

        // Stuff for best-of-N
        String[] topstrings = new String[N];
        double[] topscores = new double[N];
        for (int i = 0; i < N; i++) {
            topscores[i] = Double.NEGATIVE_INFINITY;
        }

        long etime = System.nanoTime() + (long) (time_in_seconds * 1e9);
        int found = 0;
        // Run pipeline in dummy mode until 50 values are delivered
        int left_to_ignore = 50;
        while (true) {
            if (System.nanoTime() > etime) {
                break;
            }

            String poss = pipeline.source.next();
            boolean failed = false;
            for (int i = 0; i < nfilts; i++) {
                boolean dummy = left_to_ignore > 0;
                double score = pipeline.filters[i].score(poss, dummy);
                // Crude self-tuning filter.

                double alpha = alphas[i];
                double record = maxima[i];
                maxima[i] = Math.max(score, record);

                // In the long run, we do get slight rounding error
                totals[i] += score;
                counts[i] += 1;
                // Calculate mean afterwards, so in the first case, we admit
                double mean = totals[i] / counts[i];

                double threshold = alpha * record + (1 - alpha) * mean;
                boolean pass;
                if (score >= threshold) {
                    winrate[i] += 1;
                    pass = true;
                } else {
                    pass = false;
                }
                double ratio = (double) winrate[i] / (double) counts[i];
                if (ratio > pipeline.passrates[i]) {
                    // Logarithmic search
                    alpha += 0.3 / counts[i];
                    alphas[i] = Math.min(0.99, alpha);
                } else {
                    alpha -= 0.3 / counts[i];
                    alphas[i] = Math.max(0.01, alpha);
                }

                // Drop all "dummy" sentences used to train alphas
                if (pass) {
                    continue;
                } else {
                    failed = true;
                    break;
                }

                // We ignore the _worst_ value, as that can be arbitarily bad?
            }
            if (failed) {
                continue;
            }

            // Skip if first N haven't arrived
            if (left_to_ignore > 0) {
                left_to_ignore--;
                continue;
            }

            double score = pipeline.ranker.score(poss, /*dummy*/ false);
            found++;

            // Displace next highest score. Heap "would" be appropriate
            for (int i = N - 1; i > 0; i--) {
                if (score >= topscores[i]) {
                    // Displace, move the rest all the way down.
                    double mv1 = score;
                    String mv2 = poss;
                    for (int j = i; j > 0; j--) {
                        if (topstrings[j] == null) {
                            // Nothing there yet
                            topscores[j] = mv1;
                            topstrings[j] = mv2;
                            break;
                        }
                        if (topstrings[j].equals(mv2)) {
                            // Found overlap; pick the higher score, if
                            // probability yields a difference.
                            topscores[j] = Math.max(topscores[j], mv1);
                            break;
                        }
                        double mq1 = topscores[j];
                        String mq2 = topstrings[j];
                        topscores[j] = mv1;
                        topstrings[j] = mv2;
                        mv1 = mq1;
                        mv2 = mq2;
                        if (j == 1) {
                            topscores[0] = mv1;
                            topstrings[0] = mv2;
                        }
                    }

                    break;
                }
            }
        }
//        for (int i = 0; i < nfilts; i++) {
//            System.out.format("Level %d: Ratio %f | alpha = %f | %s\n", i,
//                    (double) winrate[i] / (double) counts[i], alphas[i],
//                    pipeline.filters[i].shortName());
//        }
        int nselected = 0;
        for (int i = 0; i < N; i++) {
            nselected += topstrings[i] == null ? 0 : 1;
        }

        if (verbose) {
            for (int i = 0; i < counts.length; i++) {
                System.out.format("Stage #%d: %d ", i, counts[i]);
            }
            if (nfilts >= 1) {
                System.out.format("Output: %d Ranked: %d Final: %d\n",
                        winrate[nfilts - 1], found, nselected);
            } else {
                System.out.format("Ranked: %d Final: %d\n",
                        found, nselected);
            }
        }
        // Can be cases when found == N but there are duplicates.
        if (nselected < N) {
            topstrings = Arrays.copyOfRange(topstrings, N - nselected, N);
        }
        return topstrings;
    }

}
