/*
 * Copyright 2016 by the House Stark contributors
 * SPDX-License-Identifier: CC0-1.0
 */
package regen;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.IntBuffer;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

/**
 * A reasonably not-bad way of looking up N-grams.
 *
 * It so happens that this class absorbs the vast majority of memory. _So_: if
 * there's a byte packing scheme that lets us avoid avoid 20% memory for 1%
 * instruction count increase, take it.
 *
 * @author msto
 */
public class NGramTable {

    /* Config variables. Like a knife edge. For instance,
     * branchFactor=.50 -> 1.33 collisions/get
     * branchFactor=.60 -> 1.38 collisions/get
     * branchFactor=.62 -> 1.80 collisions/get
     * branchFactor=.70 -> 2.85 collisions/get */
    public static final double branchFactor = 0.70;
    public static final boolean isGzipping = true;

    private static final long[] hashCoef = {1012597, 1224131, 927779, 495307};
//    private static final long[] hashCoef = {38250763L, 44989727L, 47015921L, 104422897L};

    private long slotHash(int[] key) {
        /* Needs to a) be fast b) very widely disperse similar key strings */
        long h = 0;
        for (int i = 0; i < key.length; i++) {
            h += key[i] * hashCoef[i];
        }
        return h & (nslots - 1);
    }

    private final int N;
//    private int[] store;
    private long storeoffset;
    private long nslots;
    private long nelements;
    private long limit;
    private int[] ntmp;
    private int[] instrument;
    private boolean disabled;

    NGramTable(int Na) {
        if (Na <= 0) {
            throw new IllegalStateException("NGramTable can't handle things of length less than 1");
        }
        N = Na;
        nslots = 1 << 6;
        limit = (long) (nslots * branchFactor);
        ntmp = new int[N];
        nelements = 0;
        /* We abuse the fact that arrays are default zeroed */
//        store = new int[(N + 1) * nslots];
        long reqmem = 4 * (N + 1) * nslots;
        storeoffset = Util.UNSAFE.allocateMemory(reqmem);
        Util.UNSAFE.setMemory(storeoffset, reqmem, (byte) 0);

        /* TODO: remove instrument in production */
        instrument = new int[256];
        disabled = false;
    }

    public long size() {
        return nelements;
    }

    public int getN() {
        return N;
    }

    public void setEnabled(boolean enabled) {
        disabled = !enabled;
    }

    public static void fillFromFile(InputStream istream, NGramTable t) {
        int N = t.N;
        InputStream in;
        try {
            if (isGzipping) {
                in = new BufferedInputStream(new GZIPInputStream(istream, 1 << 20), 1 << 20);
            } else {
                in = new BufferedInputStream(istream, 1 << 20);
            }
        } catch (IOException ex) {
            System.err.println("GZIP input setup failed");
            System.exit(1);
            return;
        }

        ByteBuffer bb = ByteBuffer.allocate((1 << 18) * (N + 1) * 4).order(ByteOrder.nativeOrder());
        IntBuffer ib = bb.asIntBuffer();

        int nints = 0;
        try {
            while (true) {
                bb.clear();
                int amt = in.read(bb.array());
                if (amt < 0) {
                    break;
                }
                bb.flip();
                int groups = amt / (N + 1) / 4;
                int[] key = new int[N];
                for (int k = 0; k < groups; k++) {
                    int count = ib.get();
                    ib.get(key);
                    nints += N + 1;
                    /* This takes the most time */
                    t.putRaw(key, count);
                }
                ib.flip();
            }
        } catch (IOException ex) {
            // Read ended; fine
            System.out.printf("Read %d integers\n", nints);
        }
    }

    public static NGramTable loadFromFile(InputStream istream, int N) {
        NGramTable t = new NGramTable(N);
        fillFromFile(istream, t);
        return t;
    }

    public static void writeToFile(OutputStream ostream, final NGramTable t) {
        OutputStream out;
        try {
            if (isGzipping) {
                out = new BufferedOutputStream(new GZIPOutputStream(ostream, 1 << 20));
            } else {
                out = new BufferedOutputStream(ostream, 1 << 20);
            }
        } catch (IOException ex) {
            System.err.println("GZIP output setup failed");
            System.exit(1);
            return;
        }
        /* We use a lot of memory for this to sort the values, which
           saves time/space on loading. */
        int N = t.N;
//        Long[] data = new Long[(int) t.nelements];
//        int idx = 0;
//
//        for (int i = 0; i < t.nslots; i++) {
//            if (t.get(((long) i) * (N + 1) + N) != 0) {
//                data[idx] = new Long(i);
//                idx++;
//            }
////                // Count first, then rest
////                data[idx] = new int[N + 1];
////                data[idx][0] = t.get(i * (N + 1) + N);
////                for (int k = 0; k < N; k++) {
////                    data[idx][1 + k] = t.get(i * (N + 1) + k);
////                }
////                idx++;
////            }
//        }
//        /* Sorting array reduces compressed size by ~40%, if it is compressed */
//        // SKIP SORT TO SAVE TIME
////        Arrays.sort(data, new Comparator<Long>() {
////            @Override
////            public int compare(Long o1, Long o2) {
////                /* We skip the first integer, the count */
////                for (int k = 0; k < N; k++) {
////                    int r1 = t.get(o1 * (N + 1) + k);
////                    int r2 = t.get(o2 * (N + 1) + k);
////                    int r = Integer.compare(r1, r2);
////                    if (r != 0) {
////                        return r;
////                    }
////                }
////                return 0;
////            }
////        });
//
//        ByteBuffer bb = ByteBuffer.allocateDirect(data.length * (N + 1) * 4).order(ByteOrder.nativeOrder());
//        IntBuffer ib = bb.asIntBuffer();
//        for (Long loc : data) {
//            ib.put(t.get(loc * (N + 1) + N));
//            for (int k = 0; k < N; k++) {
//                ib.put(t.get(loc * (N + 1) + k));
//            }
//        }
//
//        ib.flip();
//
//        try {
//            for (int i = 0; i < (N + 1) * 4; i++) {
//                // Handle in smaller chunks, to avoid things in Java-land
//                byte[] arr = new byte[data.length];
//                bb.get(arr);
//                out.write(arr);
//            }
//            out.flush();
//            out.close();
//        } catch (IOException ex) {
//            t.err(0, "Failed to write X-grams to output stream");
//        }

        try {
            DataOutputStream dod = new DataOutputStream(out);

            for (long loc = 0; loc < t.nslots; loc++) {
                int count = t.get(loc * (N + 1) + N);
                if (count != 0) {
                    dod.writeInt(count);
                    for (int k = 0; k < N; k++) {
                        dod.writeInt(t.get(loc * (N + 1) + k));
                    }
                }
            }
            out.flush();
            out.close();
        } catch (IOException ex) {
            t.err(0, "Failed to write X-grams to output stream");
        }

    }

    public void put(int a, int val) {
        err(1, "Tried to put Z-gram into X-gram table");
        ntmp[0] = a;
        putRaw(ntmp, val);
    }

    public void put(int a, int b, int val) {
        err(2, "Tried to put Z-gram into X-gram table");
        ntmp[0] = a;
        ntmp[1] = b;
        putRaw(ntmp, val);
    }

    public void put(int a, int b, int c, int val) {
        err(3, "Tried to put Z-gram into X-gram table");
        ntmp[0] = a;
        ntmp[1] = b;
        ntmp[2] = c;
        putRaw(ntmp, val);
    }

    public void put(int a, int b, int c, int d, int val) {
        err(4, "Tried to put Z-gram into X-gram table");
        ntmp[0] = a;
        ntmp[1] = b;
        ntmp[2] = c;
        ntmp[3] = d;
        putRaw(ntmp, val);
    }

    public int increment(int a) {
        err(1, "Tried to update Z-gram into X-gram table");
        ntmp[0] = a;
        return incRaw(ntmp, 1);
    }

    public int increment(int a, int b) {
        err(2, "Tried to update Z-gram into X-gram table");
        ntmp[0] = a;
        ntmp[1] = b;
        return incRaw(ntmp, 1);
    }

    public int increment(int a, int b, int c) {
        err(3, "Tried to update Z-gram into X-gram table");
        ntmp[0] = a;
        ntmp[1] = b;
        ntmp[2] = c;
        return incRaw(ntmp, 1);
    }

    public int increment(int a, int b, int c, int d) {
        err(4, "Tried to update Z-gram into X-gram table");
        ntmp[0] = a;
        ntmp[1] = b;
        ntmp[2] = c;
        ntmp[3] = d;
        return incRaw(ntmp, 1);
    }

    public int getOrDefault(int a, int def) {
        err(1, "Tried to get Z-gram in a X-gram table");
        ntmp[0] = a;
        return getRaw(ntmp, def);
    }

    public int getOrDefault(int a, int b, int def) {
        err(2, "Tried to get Z-gram in a X-gram table");
        ntmp[0] = a;
        ntmp[1] = b;
        return getRaw(ntmp, def);
    }

    public int getOrDefault(int a, int b, int c, int def) {
        err(3, "Tried to get Z-gram in a X-gram table");
        ntmp[0] = a;
        ntmp[1] = b;
        ntmp[2] = c;
        return getRaw(ntmp, def);
    }

    public int getOrDefault(int a, int b, int c, int d, int def) {
        err(4, "Tried to get Z-gram in a X-gram table");
        ntmp[0] = a;
        ntmp[1] = b;
        ntmp[2] = c;
        ntmp[3] = d;
        return getRaw(ntmp, def);
    }

    public String describe() {
        // Using doubles to avoid overflow
        double[] mom = new double[3];
        for (int i = 1; i < instrument.length; i++) {
            for (int j = 0; j < mom.length; j++) {
                double coef = Math.pow(i, j);
                mom[j] += coef * (double) (instrument[i]);
            }
        }
        /* Reset counters */
        instrument = new int[instrument.length];
        if (mom[0] <= 0.0) {
            return String.format("%d-gram table: Never used", N);
        }
        double mean = mom[1] / mom[0];
        double mom2 = mom[2] / mom[0];
        return String.format("%d-gram table: %d lookups, %.3f +/- %.3f tests/lookup", N, (long) mom[0], mean, Math.sqrt(mom2 - mean * mean));
    }

    private void err(int Z, String text) {
        if (Z != N) {
            throw new IllegalStateException(text.replace('X', Character.forDigit(N, 10)).replace('Z', Character.forDigit(Z, 10)));
        }
    }

    private boolean matchKey(long pos, int[] keys) {
        for (int j = 0; j < N; j++) {
            if (get(pos * (N + 1) + j) != keys[j]) {
                return false;
            }
        }
        return true;
    }

    private void placeEntry(long loc, int[] keys, int count) {
        if (get(loc * (N + 1) + N) != 0) {
            throw new IllegalStateException("EE");
        }
        for (int i = 0; i < N; i++) {
            set(loc * (N + 1) + i, keys[i]);
        }
        set(loc * (N + 1) + N, count);
        nelements++;
        if (nelements > limit) {
            upscale();
        }
    }

    private void putRaw(int[] keys, int count) {
        if (disabled) {
            return;
        }

        long loc = slotHash(keys);
        /* Empty slots are signalled by 0 counts */
        while (get(loc * (N + 1) + N) != 0) {
            loc++;
            if (loc >= nslots) {
                loc -= nslots;
            }
        }
        placeEntry(loc, keys, count);
    }

    private int incRaw(int[] keys, int amount) {
        if (disabled) {
            return amount;
        }

        long loc = slotHash(keys);
        while (true) {
            if (get(loc * (N + 1) + N) == 0) {
                /* empty, place instead */
                break;
            } else if (matchKey(loc, keys)) {
                int nmt = get(loc * (N + 1) + N) + amount;
                set(loc * (N + 1) + N, nmt);
                return nmt;
            }

            loc = (loc + 1) & (nslots - 1);
        }
        placeEntry(loc, keys, amount);
        return amount;
    }

    private int getRaw(int[] keys, int def) {
        if (disabled) {
            return def;
        }

        /* primary cost of program */
        long loc = slotHash(keys) * (N + 1);
        int tried = 0;
        while (true) {
            tried++;
            int count = get(loc + N);
            if (count == 0) {
                /* nothing to see here */
                if (tried < instrument.length) {
                    instrument[tried]++;
                } else {
                    System.out.printf("N=%d Longest run: %d at pos %d\n", N, tried, slotHash(keys));
                }
                return def;
            }
            boolean matched = true;
            for (int i = 0; i < N; i++) {
                if (get(loc + i) != keys[i]) {
                    matched = false;
                    break;
                }
            }
            if (matched) {
                // Used to measure hash function quality
                if (tried < instrument.length) {
                    instrument[tried]++;
                } else {
                    System.out.printf("N=%d Longest run: %d at pos %d\n", N, tried, slotHash(keys));
                }
                return count;
            }
            loc += N + 1;
            if (loc >= nslots * (N + 1)) {
                loc -= nslots * (N + 1);
            }
        }
    }

    private void upscale() {
        /* need not be totally efficient, as rarely called */
//        if (2 * nslots * (N + 1) >= (1 << 30)) {
//            // Boost capacity when we reach the int-addressable limit.
//            limit = (int) (nslots * 0.85);
//            if (nelements < limit) {
//                return;
//            }
//        }

        long oldnslots = nslots;
        nslots *= 2;
        limit = (long) (nslots * branchFactor);

        long oldstore = storeoffset;
        long reqmem = 4 * nslots * (N + 1);

//        long freeMemorySize = ((com.sun.management.OperatingSystemMXBean) ManagementFactory
//                .getOperatingSystemMXBean()).getFreePhysicalMemorySize();
//        long totalMemorySize = ((com.sun.management.OperatingSystemMXBean) ManagementFactory
//                .getOperatingSystemMXBean()).getTotalPhysicalMemorySize();
//        long maxUse = 1500000000L;//(long) (0.10 * totalMemorySize);
//
//        // Keep 1.5 GB of safety overhead
//        if (reqmem >= freeMemorySize - maxUse) {
//            System.err.printf("OOM! (Grew %d-gram table from %d to %d to hold %d elements)\n", N, oldnslots, nslots, nelements);
//            System.err.printf("OOM! (Need %d bytes; only %d of %d total physical memory available)\n", reqmem, freeMemorySize, totalMemorySize);
//            System.exit(1);
//        }
        if (reqmem > 1000000000L) {
            System.err.printf("Allocating %d bytes for %d-gram table. Now have %d slots\n", reqmem, N, nslots);
        }
        storeoffset = Util.UNSAFE.allocateMemory(reqmem);
        Util.UNSAFE.setMemory(storeoffset, reqmem, (byte) 0);

//        int[] old = store;
//        int oldnslots = nslots;
//        nslots *= 2;
//        limit = (int) (nslots * branchFactor);
//        try {
//            store = new int[nslots * (N + 1)];
//        } catch (OutOfMemoryError ex) {
//            Runtime r = Runtime.getRuntime();
//            long bytesneeded = (long) 4 * nslots * (N + 1);
//            System.err.printf("OOM! (Grew %d-gram table from %d to %d to hold %d elements)\n", N, oldnslots, nslots, nelements);
//            System.err.printf("OOM! (Need %d bytes; only %d available)\n", bytesneeded, r.freeMemory());
//            System.exit(1);
//        } catch (NegativeArraySizeException ex) {
//            int ns = 2 * oldnslots * (N + 1);
//            long nsl = 2 * (long) oldnslots * (N + 1);
//            System.err.printf("BOOM! (Grew %d-gram table from %d to %d to hold %d elements)\n", N, oldnslots, (long) oldnslots * 2, nelements);
//            System.err.printf("BOOM! (Array slot overflow gave %d rather that %d)\n", ns, nsl);
//            System.exit(1);
//        }
        int[] tmp = new int[N];
        for (long i = 0; i < oldnslots; i++) {
            int count = Util.UNSAFE.getInt(oldstore + 4 * (i * (N + 1) + N));
            if (count == 0) {
                continue;
            }
            for (int j = 0; j < N; j++) {
                tmp[j] = Util.UNSAFE.getInt(oldstore + 4 * (i * (N + 1) + j));
            }

//            int count = old[i * (N + 1) + N];
//            if (count == 0) {
//                /* no entry there */
//                continue;
//            }
//            for (int j = 0; j < N; j++) {
//                tmp[j] = old[i * (N + 1) + j];
//            }
            long loc = slotHash(tmp);
            while (get(loc * (long) (N + 1) + (long) N) != 0) {
                loc++;
                if (loc >= nslots) {
                    loc -= nslots;
                }
            }
            for (int j = 0; j < N; j++) {
                set(loc * (N + 1) + j, tmp[j]);
            }
            set(loc * (N + 1) + N, count);
        }
        Util.UNSAFE.freeMemory(oldstore);
        if (reqmem > 1000000000L) {
            System.err.printf("Freed old memory for %d-gram table\n", N);
        }
    }

    private int get(long idx) {
//        return store[(int) idx];
        return Util.UNSAFE.getInt(storeoffset + 4 * idx);
    }

    private void set(long idx, int val) {
//        store[(int) idx] = val;
        Util.UNSAFE.putInt(storeoffset + 4 * idx, val);
    }
}
