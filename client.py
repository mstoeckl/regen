#!/usr/bin/env python3

# NOTE: Requires process-server

import os, sys, time

setcolor = "\u001b[93m"
uncolor = "\u001b[0m"

with open("server_out") as q:
    with open("server_in", "w") as o:
        q.seek(0,2)
        while True:
            i = input()
            o.write(i + "\n")
            o.flush()
            while True:
                line = q.readline().strip()
                if not line:
                    break
                else:
                    print(setcolor, line, uncolor, sep="")
