# REGEN

To understand the code, you'll have to read the source. 

Competition mode. The program behaves differently if it is not provided with 
an input pipe. More details can be found with "./stark -h".

./stark < examples.input > examples.output

Necessary Stanford CoreNLP libraries (expected to be found in dist/lib/)

slf4j-api.jar
slf4j-simple.jar
stanford-corenlp-3.6.0.jar

Necessary models for parser pipelines (also put in or link in dist/lib; they're huge)

stanford-corenlp-3.6.0-models.jar
stanford-english-corenlp-2016-01-10-models.jar
stanford-srparser-2014-10-23-models.jar

To run in competition mode, you also need to have, in a folder designated with
the environment variable GRAVEYARD,

wikipedia.gz
aclarc.gz
tagbnc.gz
tagoanc.gz

All of these can be found in cycle2.csug.rochester.edu:/localdisk/StarkP1Data
